const tabs = document.querySelectorAll('.tabs-title');
const tabContents = document.querySelectorAll('.tab-pane');

tabs.forEach(tab => {
    tab.addEventListener('click', (el) => {
        el.preventDefault();

        const tabId = tab.getAttribute('data-tab');

        tabs.forEach(tab => {
            tab.classList.remove('active');
        });
        tabContents.forEach(tabContent => {
            tabContent.classList.remove('active');
        });

        tab.classList.add('active');
        const activeTab = document.querySelector(`.tab-pane[data-tab="${tabId}"]`);
        activeTab.classList.add('active');
    });
});